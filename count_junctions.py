#! /usr/bin/env python3


from collections import namedtuple
from typing import List, NamedTuple, Sequence, Set
from os import PathLike
import pysam as ps
import pandas as pd
import click
from dataclasses import dataclass
from enum import Enum


class StrEnum(str, Enum):
    pass


EventType = StrEnum(
    "EventType", {"A3SS": "A3SS", "A5SS": "A5SS", "SE": "SE", "MXE": "MXE", "RI": "RI"}
)
Strand = StrEnum("Strand", {"+": "+", "-": "-"})
EventClass = StrEnum("EventClass", {"normal": "normal",  "cryptic": "cryptic"})


@dataclass(eq=True, order=True)
class SpliceSite:
    ENSEMBL: str
    symbol: str
    chrom: str
    strand: str
    donor: int
    acceptor: int


@dataclass(eq=True, order=True)
class MisSplicingEvent:
    normal: Sequence[SpliceSite]
    cryptic: Sequence[SpliceSite]
    etype: EventType


def read_rMATS_key(file: PathLike) -> dict[frozenset[StrEnum], Set[NamedTuple]]:
    dict_list = (
        pd.read_csv(file, sep="\t")
        .apply(
            lambda x: {
                frozenset(
                    {  # type: ignore
                        EventType(x["rMATSevent"]),
                        Strand(x["strand"]),
                        EventClass(x["molecular_event"]),
                    }
                ): {"donor": x["donor"], "acceptor": x["acceptor"]}
            },
            axis=1,
            result_type="reduce",
        )
        .to_list()
    )
    nd = {}
    SitesPair = namedtuple("SitePair", ["donor", "acceptor"])
    for d in dict_list:
        if len(d.keys()) > 1:
            raise ValueError("Each dict in the list must have only one key")
        for k, v in d.items():
            if k not in nd.keys():
                nd[k] = set()
            nd[k].add(SitesPair(donor=v["donor"], acceptor=v["acceptor"]))
    return nd


# TODO: add support for RI. Fake normal as coverage of the middle if intron
def parse_splice_table(
    table: str, rMATSkey: PathLike, t: EventType
) -> dict[str, MisSplicingEvent]:
    """ """

    key = read_rMATS_key(file=rMATSkey)

    splice_table: List = (
        pd.read_csv(table, sep="\t")  # type: ignore
        .apply(
            lambda x: MisSplicingEvent(  # type: ignore
                normal=[
                    SpliceSite(
                        ENSEMBL=x["GeneID"],
                        symbol=x["geneSymbol"],
                        chrom=str(x["chr"]),
                        strand=x["strand"],
                        donor=x[donor],
                        acceptor=x[acceptor],
                    )
                    for donor, acceptor in key[
                        frozenset(
                            {
                                EventType(t),
                                Strand(x["strand"]),
                                EventClass("normal"),
                            }
                        )
                    ]
                ],
                cryptic=[
                    SpliceSite(
                        ENSEMBL=x["GeneID"],
                        symbol=x["geneSymbol"],
                        chrom=str(x["chr"]),
                        strand=x["strand"],
                        donor=x[donor],
                        acceptor=x[acceptor],
                    )
                    for donor, acceptor in key[
                        frozenset(
                            {
                                EventType(t),
                                Strand(x["strand"]),
                                EventClass("cryptic"),
                            }
                        )
                    ]
                ],
                etype=t,
            ),
            axis=1,
            result_type="reduce",
        )
        .to_list()
    )
    for mse in splice_table:
        print(sorted(mse.normal))
    return {
        f"{mse.normal[0].symbol}_{'_'.join([str(se.acceptor) for se in sorted(mse.normal)])}": mse
        for mse in splice_table
    }


def count_spliced_reads(
    splice_site: SpliceSite,
    bam: str,
    sam_options: dict,
    no_match_partner: bool,
    etype: EventType = EventType("A3SS"),
) -> dict[str, set[str]]:
    # open the BAM file
    with ps.AlignmentFile(  # type: ignore
        bam,
        "rb",
    ) as samfile:
        junctions = samfile.count_junction(
            read_iterator=(
                read
                for read in samfile.fetch(
                    # Pysam uses half-open intevals: [start, end, hence end+1
                    splice_site.chrom,
                    splice_site.acceptor,
                    splice_site.acceptor + 1,
                )
            ),
            donor_site=splice_site.donor,
            acceptor_site=splice_site.acceptor,
            forward_strand=splice_site.strand == "+",
            sam_options=sam_options,
            etype=str(etype),
            no_match_splice_partner=no_match_partner,
        )
        return junctions


@click.command()
@click.option(
    "--barcodes",
    "-bc",
    required=True,
    help="""
    Path to the barcodes file.
    Typically [cellranger output]/outs/filtered_feature_bc_matrix/barcodes.tsv.gz
    """,
)
@click.option(
    "--bam",
    "-b",
    required=True,
    help="""
    Path to the bam file. Must be indexed.
    Typically [cellranger output]/outs/possorted_genome_bam.bam
    """,
)
@click.option(
    "--table",
    "-m",
    required=True,
    help="""
    \b
    Path to the table with splice sites. Must have the following columns:
       GeneID
       geneSymbol
       chr
       strand
       shortES
       shortEE
       longExonStart_0base
       longExonEnd
    """,
)
@click.option(
    "--rmats-key",
    "-r",
    default="data/rMATS_key.tsv",
    help="Table connecting th Mis-Splicing event types to rMATS columns for donor and acceptor sites",
)
@click.option(
    "--output",
    "-o",
    required=True,
    help="Output directory",
)
@click.option(
    "--no-match-splice-partner",
    "-np",
    is_flag=True,
    help="Flag to skip matching the splice partner when parsing the BAM file. Only relevant for A3SS (skips matching donor) and A5SS (skips matching acceptor)",
)
@click.option("--cb-tag", default="CB", help="SAM tag used for cell barcode")
@click.option("--umi-tag", default="UB", help="SAM tag used for UMI sequence")
@click.option("--no-umi", is_flag=True, help="Flag for data that doesn't have UMIs")
@click.option(
    "--event-type",
    "-et",
    default="A3SS",
    help="""
        \b
        The type of event present. Currently supported types:
            A3SS
            A5SS - unimplemented
            SE - unimplemented
            MXE - unimplemented
        """,
)
def main(
    barcodes,
    bam,
    table,
    rmats_key,
    output,
    no_match_splice_partner,
    cb_tag,
    umi_tag,
    no_umi,
    event_type,
):
    if not no_umi:
        sam_options = {"CB": cb_tag, "UMI": umi_tag}
    else:
        sam_options = {"CB": cb_tag, "UMI": None}

    st = parse_splice_table(table=table, rMATSkey=rmats_key, t=EventType(event_type))

    ## read the barcodes file as a pandas dtaframe and extract the barcodes column
    barcode_list: List[str] = list(
        pd.read_csv(
            barcodes,
            sep="\t",
            header=None,
        )[0]
    )

    normal_dict = {
        f"{gene}_SS{str(i)}": [0 for _ in barcode_list]
        for gene in st
        for i, ss in enumerate(st[gene].normal)
    }
    cryptic_dict = {
        f"{gene}_SS{str(i)}": [0 for _ in barcode_list]
        for gene in st
        for i, ss in enumerate(st[gene].cryptic)
    }

    # TODO: this whole thing might be better organised as HDF5 on both python and R side
    for gene in st:
        umi_counts_norm = [
            count_spliced_reads(
                ss, bam, sam_options, no_match_splice_partner, st[gene].etype
            )
            for ss in st[gene].normal
        ]
        umi_counts_crypt = [
            count_spliced_reads(
                ss, bam, sam_options, no_match_splice_partner, st[gene].etype
            )
            for ss in st[gene].cryptic
        ]

        for i, umi_dict in enumerate(umi_counts_norm):
            for bc in umi_dict:
                if bc in barcode_list:
                    normal_dict[f"{gene}_SS{str(i)}"][barcode_list.index(bc)] = len(
                        umi_dict[bc]
                    )
        for i, umi_dict in enumerate(umi_counts_crypt):
            for bc in umi_dict:
                if bc in barcode_list:
                    cryptic_dict[f"{gene}_SS{str(i)}"][barcode_list.index(bc)] = len(
                        umi_dict[bc]
                    )

    df_norm = pd.DataFrame(data=normal_dict, index=pd.Index(barcode_list, dtype = 'str'))
    df_crypt = pd.DataFrame(data=cryptic_dict, index=pd.Index(barcode_list, dtype = 'str'))

    df_norm.to_csv(
        path_or_buf=f"{output}_normal.tsv.gz", sep="\t", index_label="Barcode"
    )
    df_crypt.to_csv(
        path_or_buf=f"{output}_cryptic.tsv.gz", sep="\t", index_label="Barcode"
    )

    for k in cryptic_dict:
        print(k)


if __name__ == "__main__":
    main()
