# Pileup and barcode aggregation for specific splice sites

Script to parse BAM files and identify mis-splicing events for a pre-defined list of cryptic splice sites
