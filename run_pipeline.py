#! /usr/bin/env python3


from typing import List
from os import PathLike
import pandas as pd
import click
import itertools
import subprocess
from concurrent.futures import ProcessPoolExecutor
import os


def create_command(
    row, ms_table, rmats_key, event_type, no_partner, out, cb_tag, umi_tag, no_umi
):
    return " ".join(
        [
            "count_junctions.py",
            f"-bc",
            f"{row['barcodes']}",
            f"-b",
            f"{row['bam']}",
            f"-m",
            f"{ms_table}",
            f"-r",
            f"{rmats_key}",
            f"-et",
            f"{event_type}",
            f"-o",
            f"{out}/splice_counts/{row['name']}_{event_type}",
            f"--cb-tag",
            f"{row['cb-tag']}" if "cb-tag" in row.index else f"{cb_tag}",
            f"--umi-tag",
            f"{row['umi-tag']}" if "umi-tag" in row.index else f"{umi_tag}",
        ]
        + ([f"--no-umi"] if no_umi else [])
        + ([f"-np"] if no_partner else [])
    )


def run_command(command):
    subprocess.run(command, shell=True)


@click.command()
@click.option(
    "--samples",
    "-s",
    required=True,
    help="""
    \b
    Path to the sample table TSV. Must have the following columns:
        name
        bam
        barcodes
        mut
    """,
)
@click.option(
    "--table",
    "-m",
    multiple=True,
    required=True,
    help="""
    \b
    Path to the table with splice sites.
    Must have the following columns:
       GeneID
       geneSymbol
       chr
       strand
       Additional columns corresponding to the splice site positions.
       Exact column names depend on the rMATS key provided with `--rmats-key`
    """,
)
@click.option(
    "--rmats-key",
    "-r",
    default="data/rMATS_key.tsv",
    help="Table connecting th Mis-Splicing event types to rMATS columns for donor and acceptor sites.",
)
@click.option(
    "--event-type",
    "-et",
    multiple=True,
    default=["A3SS"],
    help="""
        \b
        The type of event present. Currently supported types:
            A3SS
            A5SS
            SE
            MXE
        """,
)
@click.option(
    "--no-match-splice-partner",
    "-np",
    is_flag=True,
    help="Flag to skip matching the splice partner when parsing the BAM file. Only relevant for A3SS (skips matching donor) and A5SS (skips matching acceptor).",
)
@click.option(
    "--outdir",
    "-o",
    required=True,
    help="Output directory.",
)
@click.option("--nproc", "-n", default=1, help="Number of parallel processes to use.")
@click.option(
    "--threshold",
    "-t",
    default=0.01,
    help="FDR (lower boundary) threshold for the model predictions.",
)
@click.option(
    "--frd-th",
    default=0.05,
    help="Threshold for fractional reative difference to filter splice sites based on bulk group differences.",
)
@click.option(
    "--min-features",
    default=2,
    help="Minimum number of features present in cell to consider it for classification.",
)
@click.option("--cb-tag", default="CB", help="SAM tag used for cell barcode.")
@click.option("--umi-tag", default="UB", help="SAM tag used for UMI sequence.")
@click.option("--no-umi", is_flag=True, help="Whether or not data has UMIs.")
@click.option(
    "--benchmark-frd-th",
    is_flag=True,
    help="Whether to use different FRD cutoffs to plot how it affects the performance.",
)
@click.option(
    "--regression-type", default="mlr", help="Regression type. One of 'mlr' or 'slr'."
)
@click.option(
    "--split-bams", is_flag=True, help="Whether to create separate bam files "
)
def main(
    samples: PathLike,
    table: List[PathLike],
    rmats_key: PathLike,
    event_type: List[str],
    no_match_splice_partner: bool,
    outdir: PathLike,
    nproc: int,
    threshold: float,
    cb_tag: str,
    umi_tag: str,
    frd_th: float,
    min_features: int,
    no_umi: bool,
    benchmark_frd_th: bool,
    regression_type: str,
    split_bams: bool,
):
    os.makedirs(f"{outdir}/splice_counts", exist_ok=True)
    os.makedirs(f"{outdir}/classification", exist_ok=True)
    sample_table: pd.DataFrame = pd.read_csv(samples, sep="\t", dtype="str")
    commands = sample_table.apply(
        func=lambda x: [
            create_command(
                row=x,
                ms_table=tbl,
                rmats_key=rmats_key,
                event_type=et,
                no_partner=no_match_splice_partner,
                out=outdir,
                cb_tag=cb_tag,
                umi_tag=umi_tag,
                no_umi=no_umi,
            )
            for tbl, et in zip(table, event_type)
        ],
        # TODO: this is ugly and unreadable
        axis=1,
    ).to_list()

    commands_flat = list(itertools.chain.from_iterable(commands))

    with ProcessPoolExecutor(max_workers=nproc) as executor:
        futures = executor.map(run_command, commands_flat)

    subprocess.run(
        args=[
            "Splicing.R",
            "--samples",
            f"{samples}",
            "--countdir",
            f"{outdir}/splice_counts",
            "--outdir",
            f"{outdir}/classification",
            "--threshold",
            f"{threshold}",
            "--frd-th",
            f"{frd_th}",
            "--min-features",
            f"{min_features}",
            "--regression-type",
            f"{regression_type}",
            "--n-proc",
            f"{nproc}",
            "--cb-tag",
            f"{cb_tag}",
        ]
        + ([f"--benchmark-frd-th"] if benchmark_frd_th else [])
        + ([f"--split-bams"] if split_bams else [])
        + ["--event-type"]
        + [str(et) for et in event_type]
    )


if __name__ == "__main__":
    main()
